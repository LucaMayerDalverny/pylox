import sys

def run_file(path : str):
    print("[Warning] running lox code from file is not implemented yet")

def run_prompt():
    
    while True:
        line = input("> ")
        if not line:
            break
        print("[Warning] lox code line evaluation is not implemented yet")

if __name__ == "__main__":

    args = sys.argv[1:]

    if len(args) > 1:
        print("Usage: pylox [script]")
        exit(64)
    elif len(args) == 1:
        run_file(args)
    else:
        run_prompt()
